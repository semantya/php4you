<?php
namespace PHP2USE\Gateway\restler;

use PHP2USE\Site;
use PHP2USE\Common as Common;

class Platform extends Common\Platform {
    private static $inst = null;
    private static $apis = array();
    
    public static function bootstrap () {
        use Luracast\Restler\Restler;
        
        Platform_Restler::$inst = new Restler();
        
        Platform_Restler::$inst->setSupportedFormats('JsonFormat','HtmlFormat');
    }
    
    public static function lunch () {
        foreach (Platform_Restler::$apis as $api) {
            Platform_Restler::$inst->addAPIClass($api);
        }
        
        Platform_Restler::$inst->handle();
    }
    
    public static function render ($tpl, $args=array()) {
    }
}

