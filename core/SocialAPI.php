<?php
namespace PHP2USE\APIs;

use PHP2USE;
use PHP2USE\Cache;
use PHP2USE\Common as Common;

class ReflectionException extends Common\SocialException {
    public $verb;
    public $method;
    public $args;
    public $resp;
    
    protected function initialize ($verb, $method, $args, $resp) {
        $this->verb   = $verb;
        $this->method = $method;
        $this->args   = $args;
        $this->resp   = $resp;
    }
    
    public function render () {
        
    }
}

class SocialAPI extends Common\Helper {
    protected $creds;
    protected $cfg;
    protected $vault;
    
    protected $cache;
    
    public function __construct ($key, $creds, $cfg=array(), $vault=array()) {
        $this->name  = $key;
        $this->creds = $creds;
        $this->cfg   = $cfg;
        
        $this->vault = new Cache\Wrapper("apis://vault@%s/%s", $vault, array($this->name));
        $this->cache = new Cache\Wrapper("apis://cache:%s@%s/%s?%s");
    }
    
    public function name      () { return $this->name; }
    public function classname () { return "\\PHP2USE\\APIs\\".ucfirst($this->name); }
    public function title     () { return ucfirst($this->name); }
    
    public function rewrite_ns ($ns) {
        return implode('\\', array($this->classname(), $ns));
    }
    
    public function reflect($ns, $resp, $callback) {
        return $this->wrap($ns, $resp, $callback);
    }
    
    public function invoke($verb, $link, $args=array()) {
        $nrw = array($verb, $this->name, $link, http_build_query($args));
        
        if ($this->cache->has($nrw)) {
            return $this->cache->get($nrw);
        } else {
            $resp = $this->call($verb, $link, $args);
            
            if ($resp!=null) {
                return $this->cache->set($nrw, $resp);
            } else {
                return null;
            }
        }
    }
    
    protected function call_http ($verb, $url, $payload=array()) {
        $chandle = curl_init();
        curl_setopt($chandle, CURLOPT_URL, $url);
        curl_setopt($chandle, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chandle);
        curl_close($chandle);
        
        return $result;
    }
    protected function call_json ($verb, $url, $payload=array()) {
        $resp = $this->call_http($verb, $url, $payload);
        
        $data = json_decode($resp);
        
        return $data;
    }
    protected function call_xml ($verb, $url, $payload=array()) {
        $resp = $this->call_http($verb, $url, $payload);
        
        $data = new \SimpleXMLElement(html_entity_decode($resp));
        
        return $data;
    }
    
    public function features() {
        $mpp = array(
            'google'   => array('oauth','social','media'),
            'facebook' => array('oauth','social','insights','media'),
            'twitter'  => array('oauth','social'),
            'flickr'   => array('oauth','social','media'),
        );
        
        if (array_key_exists($this->name(), $mpp)) {
            return $mpp[$this->name()];
        } else {
            return array();
        }
    }
    
    public function supports($key) {
        return in_array($key, $this->features());
    }
}

class SocialObject extends Common\Helper {
    
}

class SocialResource extends SocialObject {
    protected $prn;
    protected $nrw;
    protected $res;
    
    public function __construct ($prn, $nrw, $res) {
        $this->prn = $prn;
        $this->nrw = $nrw;
        $this->res = $res;
        
        $this->initialize();
    }
    
    public function rewrite_ns ($ns) {
        return $this->prn->rewrite_ns($ns);
    }
    
    protected function call() { return call_user_func_array(array($this->prn, 'invoke'), func_get_args()); }
}

