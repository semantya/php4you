#!/usr/bin/env python

RES_PATH = 'php2use/resources/%s'

import os, sys

def shell(*cmd):
    def shell_wrap(x):
        if ' ' in x:
            return '"%s"' % x
        else:
            return x
    
    os.system(' '.join([
        shell_wrap(x)
        for x in cmd
    ]))

def parse_call(prg, cmd, *args):
    if cmd=='init':
        pass
    
    elif cmd=='env':
        for src,dest in [
            ('htaccess',         '.htaccess'),
            ('composer.json',    None),
            ('doctrine-cli.php', 'cli-config.php'),
            ('git.ignore',       '.gitignore'),
        ]:
            shell('cp', '-afR', RES_PATH % src, dest or src)
        
        shell(RES_PATH % 'composer.phar', 'update')
    
    elif cmd=='commit':
        shell('git', 'commit', '-a')
        
        #parse_call(*sys.argv)
        
        shell('git', 'push', '-u', 'devel', 'master')
    
    elif cmd=='upgrade':
        shell(RES_PATH % 'composer.phar', 'self-update')
        
        os.chdir('php2use')
        
        shell('git', 'add', '--all')
        
        shell('git', 'commit', '-a', '-m', "Upgraded the php2use framework ...")
        
        shell('git', 'pull', '-u', 'devel', 'master')
        
        shell('git', 'push', '-u', 'devel', 'master')
    
    else:
        print "Command not supported : %s" % cmd

if __name__=='__main__':
    parse_call(*sys.argv)

