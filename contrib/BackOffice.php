<?php
namespace PHP2USE\Contrib\BackOffice;

use PHP2USE;
use PHP2USE\Site;
use PHP2USE\Common as Common;

class Manager extends Common\Component {
    public static function bootstrap () {
        
    }
    
    /***************************************************************************************************/
    
    public static function register_routing () {
        Site::map('/admin/',              array('PHP2USE\\Contrib\\BackOffice\\Manager','view_dashboard'),  'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
        Site::map('/admin/i18n/?',        array('PHP2USE\\Contrib\\BackOffice\\Manager','view_trans'),      'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
        Site::map('/admin/settings/?',    array('PHP2USE\\Contrib\\BackOffice\\Manager','view_settings'),   'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
        
        Site::map('/admin/explorer',      array('PHP2USE\\Contrib\\BackOffice\\Manager','view_explorer'),   'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
        Site::map('/admin/explorer/rpc',  array('PHP2USE\\Contrib\\BackOffice\\Manager','view_fs_rpc'),     'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
        
        Site::map('/admin/pages/?',       array('PHP2USE\\Contrib\\BackOffice\\Manager','view_page_entry'), 'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
        Site::map('/admin/pages/?',       array('PHP2USE\\Contrib\\BackOffice\\Manager','view_page_list'),  'admin', null, array('guest','PHP2USE\\Contrib\\BackOffice\\Manager'), true);
    }
    
    /**************************************************************************/
    
    public static function view_dashboard () {
        return CMS::current()->render_page("dashboard");
    }
    
    /**************************************************************************/
    
    public static function view_trans () {
        return CMS::current()->render_page("i18n", array(
            'listing' => i18n::query(),
        ));
    }
    
    /**************************************************************************/
    
    public static function view_settings () {
        return CMS::current()->render_page("settings");
    }
    
    /**************************************************************************/
    
    public static function view_explorer () {
        return CMS::current()->render_page("explorer");
    }
    public static function view_fs_rpc () {
        include_once PHP2USE_ROOT."/contrib/admin/files/lib/autoload.php";
        
        // Required for MySQL storage connector
        // include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeMySQL.class.php';
        // Required for FTP connector support
        // include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeFTP.class.php';
        
        /**
         * Simple function to demonstrate how to control file access using "accessControl" callback.
         * This method will disable accessing files/folders starting from '.' (dot)
         *
         * @param  string  $attr  attribute name (read|write|locked|hidden)
         * @param  string  $path  file path relative to volume root directory started with directory separator
         * @return bool|null
         **/
        function access($attr, $path, $data, $volume) {
	        return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
		        ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
		        :  null;                                    // else elFinder decide it itself
        }
        
        $mpath = PHP2USE_ROOT."/../media/";
        
        $opts = array(
	        // 'debug' => true,
	        'roots' => array(
		        array(
			        'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
			        'path'          => "{$mpath}/",         // path to files (REQUIRED)
			        'URL'           => dirname($_SERVER['PHP_SELF']) . "{$mpath}/", // URL to files (REQUIRED)
			        'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
		        )
	        )
        );
        
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();
    }
    
    /**************************************************************************/
    
    public static function view_page_list () {
        return CMS::current()->render_page("page/list");
    }
    public static function view_page_entry ($pattern) {
        return CMS::current()->render_page("page/entry");
    }
}

