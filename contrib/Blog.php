<?php
namespace PHP2USE\Contrib\Blog;

use PHP2USE;
use PHP2USE\Common as Common;

class Manager extends Common\Component {
    public static function bootstrap () {
        
    }
    
    /***************************************************************************************************/
    
    public static function register_routing () {
        Site::map('/blog',               array('PHP2USE\\Contrib\\Blog\\Manager','view_home'));
        Site::map('/blog/',              array('PHP2USE\\Contrib\\Blog\\Manager','view_home'));
        Site::map('/blog/([0-9]+)',      array('PHP2USE\\Contrib\\Blog\\Manager','view_entry'));
        Site::map('/blog/([0-9]+)-(.+)', array('PHP2USE\\Contrib\\Blog\\Manager','view_entry'));
    }
    
    /**************************************************************************/
    
    public static function view_home () {
        Site::render_page("blog/home");
    }
    public static function view_categ ($cid) {
        Site::render_page("blog/category");
    }
    public static function view_entry ($uid, $slug) {
        $doc = ORM::get_doc();
        
        Site::render_page("blog/entry", array(
            'post' => $doc,
        ));
    }
    
    /**************************************************************************/
    
    
}

