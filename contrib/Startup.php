<?php
namespace PHP2USE\Contrib\Startup;

use PHP2USE\Site;
use PHP2USE\Common as Common;
use PHP2USE\Contrib\SSO;

class TeamMember {
    private $pseudo;
    public $visible;
    public $role;
    public $full;
    public $title;
    public $bio;

    private $attrs;
    private $social=array();

    public function __construct ($pseudo, $role, $full, $visible, $attrs) {
        $this->pseudo = $pseudo;
        $this->role   = $role;
        $this->full   = $full;
        $this->visible = $visible;
        $this->attrs   = $attrs;
    }

    public function attr ($key, $default=null) {
        if (array_key_exists($key, $this->attrs)) {
            return $this->attrs[$key];
        } else {
            return $default;
        }
    }

    public function enrich ($key, $link) {
        if (!array_key_exists($key, $this->social)) {
            $this->social[] = array(
                'key'   => $key,
                'label' => ucfirst($key),
                'link'  => $link,
            );
        }
    }

    public function mugshot () {
        return "/media/mugshot/{$this->pseudo}.jpg";
    }

    public function aura () {
        return $this->social;
    }

    public function active () {
        return $this->visible or SSO\Manager::has_scopes('tester');
    }
}

class TeamRef {
    private $key;
    private $social=array();
    private $dns=array();

    public $title;
    public $visible;
    public $bio;
    public $tech=array();

    public function __construct ($key, $title, $visible=false) {
        $this->key     = $key;
        $this->title   = $title;
        $this->visible = $visible;
    }

    public function extend ($fqdn) {
        if (!in_array($fqdn, $this->dns)) {
            $this->dns[] = $fqdn;
        }

        return $this;
    }

    public function enrich ($key, $link) {
        if (!array_key_exists($key, $this->social)) {
            $this->social[$key] = array(
                'key'   => $key,
                'label' => ucfirst($key),
                'link'  => $link,
            );
        }

        return $this;
    }

    public function name () { return $this->key; }
    public function logo () {
        return "/media/logos/".$this->name().".png";
    }

    public function domain () {
        if (sizeof($this->dns)) {
            return $this->dns[0];
        } else {
            return null;
        }
    }

    public function link () {
        if (sizeof($this->dns)) {
            return "http://{$this->dns[0]}/";
        } else if (sizeof($this->social)) {
            return $this->aura()[0]['link'];
        } else {
            return '#';
        }
    }

    public function aura () {
        return array_values($this->social);
    }

    public function tags ($pattern) {
        $resp = array();

        foreach ($this->tech as $tag) {
            $resp[] = sprintf($pattern, $tag, ucfirst($tag));
        }

        return implode(' ', $resp);
    }

    public function active () {
        return $this->visible or SSO\Manager::has_scopes('tester');
    }

    public function is_video () {
        if (sizeof($this->social)==1 and array_key_exists('youtube', $this->social)) {
            return true;
        }

        return false;
    }

    public function yt_uid () {
        if ($this->is_video()) {
            $resp = $this->social['youtube']['link'];

            foreach (array(
                'www.youtube.com', 'm.youtube.com', 'youtube.com','youtu.be',
                'https://', 'http://', '/watch?v=',
            ) as $tag) {
                $resp = str_replace($tag, '', $resp);
            }

            return $resp;
        } else {
            return "";
        }
    }

    public function yt_cover () {
        if ($this->is_video()) {
            return "http://img.youtube.com/vi/{$this->yt_uid()}/mqdefault.jpg";
        } else {
            return null;
        }
    }

    public function yt_thumbs () {
        $resp = array();

        if ($this->is_video()) {
            for ($i=0 ; $i<4 ; $i++) {
                $resp[] = "http://img.youtube.com/vi/{$this->yt_uid()}/{$i}.jpg";
            }
        }

        return $resp;
    }
}

Site::set('team',      array());
Site::set('portfolio', array());

class Manager extends Common\Component {
    public static function bootstrap () {

    }

    /***************************************************************************************************/

    public static function register_routing () {
        Site::map('/team',           array('PHP2USE\\Contrib\\Startup\\Manager','view_team'));
        Site::map('/portfolio',      array('PHP2USE\\Contrib\\Startup\\Manager','view_portfolio'));
        Site::map('/pricing',        array('PHP2USE\\Contrib\\Startup\\Manager','view_pricing'));
        Site::map('/testimonials',   array('PHP2USE\\Contrib\\Startup\\Manager','view_testimonials'));
    }

    //************************************************************************//

    public static function view_profile () {
        Site::render_page("profile");
    }
    public static function view_pricing () {
        Site::render_page("pricing");
    }
    public static function view_testimonials () {
        Site::render_page("testimonials");
    }

    //************************************************************************//

    public static function view_product ($slug) {
        Site::render_page("product/{$slug}");
    }
    public static function view_service ($slug) {
        Site::render_page("service/{$slug}");
    }

    //************************************************************************//

    public static function add_member ($pseudo, $role, $full, $visible=true, $attrs=array()) {
        if (!array_key_exists($pseudo, Site::$cfg['team'])) {
            Site::$cfg['team'][$pseudo] = new TeamMember($pseudo, $role, $full, $visible, $attrs);
        }

        return Site::$cfg['team'][$pseudo];
    }
    public static function members () {
        $resp = array();

        foreach (Site::$cfg['team'] as $pseudo => $mbr) {
            $resp[] = $mbr;
        }

        return $resp;
    }
    public static function view_team () {
        Site::render_page("team");
    }

    //************************************************************************//

    public static function add_ref ($state, $key, $title) {
        if (!array_key_exists($key, Site::$cfg['portfolio'])) {
            Site::$cfg['portfolio'][$key] = new TeamRef($key, $title, $state);
        }

        return Site::$cfg['portfolio'][$key];
    }
    public static function references () {
        $resp = array();

        foreach (Site::$cfg['portfolio'] as $key => $ref) {
            $resp[] = $ref;
        }

        return $resp;
    }

    //************************************************************************//

    public static function technologies () {
        $resp = array();

        foreach (Site::$cfg['portfolio'] as $key => $ref) {
            if ($ref->visible) {
                foreach ($ref->tech as $tag) {
                    if (!in_array($tag, $resp)) {
                        $resp[] = $tag;
                    }
                }
            }
        }

        return $resp;
    }

    public static function view_portfolio () {
        Site::render_page("portfolio");
    }
}
