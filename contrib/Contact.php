<?php
namespace PHP2USE\Contrib\Contact;

use PHP2USE;
use PHP2USE\Common as Common;

class Manager extends Common\Component {
    public static function bootstrap () {
        
    }
    
    /***************************************************************************************************/
    
    public static function register_routing() {
        Site::map('GET /contact',  array('PHP2USE\\Contrib\\ContactForm\\Manager','get'));
        Site::map('POST /contact', array('PHP2USE\\Contrib\\ContactForm\\Manager','post'));
    }
    
    /**************************************************************************/
    
    public static function get () {
        Site::render_page("contact");
    }
    public static function post () {
        Site::render_page("contact");
    }
    
    /**************************************************************************/
    
    private static function show_form () {
        
    }
}

