    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Please Sign In
                        <a class="pull-right" href="/sso/auth/openid" title="OpenID">
                            &nbsp;<i class="fa fa-key"></i>&nbsp;
                        </a>
<?php foreach (SSO::providers() as $api) { ?>
                        <a class="pull-right" href="/sso/auth/<?php echo $api->name() ?>" title="<?php echo $api->title() ?>">
                            &nbsp;<i class="fa fa-<?php echo $api->name() ?>"></i>&nbsp;
                        </a>
<?php } ?>
                        </h3>
                    </div>
                    <div class="panel-body">
<?php foreach ($sso_notify as $msg) { ?>
                        <div class="alert alert-<?php echo $msg['level'] ?>">
                            <?php echo $msg['text'] ?>
                        </div>
<?php } ?>
                        <form method="POST" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <input type="Submit" class="btn btn-lg btn-success btn-block" value="Login" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

