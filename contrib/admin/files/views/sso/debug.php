<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Debug your SSO</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    SESSION
                    <a class="pull-right label label-success" target="_blank" href="/sso/login">Login</a>
                    <a class="pull-right label label-danger" target="_blank" href="/sso/logout">Logout</a>
                </div>
                <div class="panel-body">
                    <p>
<?php foreach (array('opauth','nsap.email','nsap.payload') as $key) { ?>
                        <button type="button" class="btn btn-outline btn-default"><?php echo ucfirst($key) ?></button>
<?php if (isset($_SESSION[$key])) { ?>
                        <button type="button" class="btn btn-outline btn-primary"><?php print_r(Site::platform()->session_get($key)) ?></button>
<?php } else { ?>
                        <button type="button" class="btn btn-outline btn-danger">Value not assigned</button>
<?php } ?>
                        <br />
<?php } ?>
                    </p>
                </div>
                <div class="panel-body">
                    <p>
<?php foreach (SSO::scopes() as $scope) { ?>
                        <button type="button" class="btn btn-outline btn-<?php echo ($scope['callback'](SSO::email())) ? 'success' : 'danger'; ?>">
                            <?php echo ucfirst($scope['name']) ?>
                        </button>
<?php } ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

