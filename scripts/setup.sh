#!/bin/bash

TARGET_DIR=$1
FRAMEWORK_DIR=$TARGET_DIR/php2use
RESOURCE_DIR=$FRAMEWORK_DIR/resources

cd $FRAMEWORK_DIR

git pull origin master

git submodule update --init --recursive

cd $TARGET_DIR

if [[ ! -f "$TARGET_DIR/composer.json" ]] ; then
    cp $RESOURCE_DIR/composer.json $TARGET_DIR/composer.json

    if [[ `whereis composer`!="composer:" ]] ; then
        composer install
    else
        $RESOURCE_DIR/composer.phar self-update

        $RESOURCE_DIR/composer.phar install
    fi
fi

if [[ ! -f "$TARGET_DIR/.gitignore" ]] ; then
    cp $RESOURCE_DIR/git.ignore $TARGET_DIR/.gitignore
fi

if [[ ! -f "$TARGET_DIR/.htaccess" ]] ; then
    cp $RESOURCE_DIR/htaccess $TARGET_DIR/.htaccess
fi

